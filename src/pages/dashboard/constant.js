export const dummy = [
  {
    id: 0,
    imageUrl: 'assets/img/POSTER-THE-TEMPLE-OF-RIDDLE-OK-scaled.jpg',
    title: 'The Temple of Rddle',
    genre: ['Advanture', 'Action'],
    rating: 4,
    status: 'ready', // ready / comingSoon / purchase
  },
  {
    id: 0,
    imageUrl: 'assets/img/POSTER-THE-TEMPLE-OF-RIDDLE-OK-scaled.jpg',
    title: 'The Temple of Rddle',
    genre: ['Advanture', 'Action'],
    rating: 4,
    status: 'ready', // ready / comingSoon / purchase
  },
  {
    id: 0,
    imageUrl: 'assets/img/POSTER-THE-TEMPLE-OF-RIDDLE-OK-scaled.jpg',
    title: 'The Temple of Rddle',
    genre: ['Advanture', 'Action'],
    rating: 4,
    status: 'comingSoon', // ready / comingSoon / purchase
  },
  {
    id: 0,
    imageUrl: 'assets/img/POSTER-THE-TEMPLE-OF-RIDDLE-OK-scaled.jpg',
    title: 'The Temple of Rddle',
    genre: ['Advanture', 'Action'],
    rating: 4,
    status: 'comingSoon', // ready / comingSoon / purchase
  },
  {
    id: 0,
    imageUrl: 'assets/img/POSTER-THE-TEMPLE-OF-RIDDLE-OK-scaled.jpg',
    title: 'The Temple of Rddle',
    genre: ['Advanture', 'Action'],
    rating: 4,
    status: 'purchase', // ready / comingSoon / purchase
  },
  {
    id: 0,
    imageUrl: 'assets/img/POSTER-THE-TEMPLE-OF-RIDDLE-OK-scaled.jpg',
    title: 'The Temple of Rddle',
    genre: ['Advanture', 'Action'],
    rating: 4,
    status: 'purchase', // ready / comingSoon / purchase
  },
];
