import React from 'react';
import clsx from 'clsx';
import Achievement from '../../../assets/svg/Achievement';

const component = ({ classes, rating }) => {
  console.log(rating);

  let rates = [];

  for (let i = 1; i <= 5; i++) {
    if (i <= rating) {
      rates.push(
        <Achievement className={clsx(classes.icon, classes.iconActive)} />
      );
    } else {
      rates.push(<Achievement className={classes.icon} />);
    }
  }

  return rates;
};

export default component;
