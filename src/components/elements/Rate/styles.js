const styles = {
  icon: {
    color: '#BDBDBD',
    width: '20px',
    height: '20px',
    marginRight: '5px',
  },
  iconActive: {
    color: '#FFBA00 !important',
  },
};

export default styles;
