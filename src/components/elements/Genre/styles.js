const styles = {
  button: {
    borderRadius: '50px',
    background: '#2F2D51',
    marginRight: '10px',
    color: '#fff',
    padding: '6px 12px',
  },
};

export default styles;
