import axios from 'axios';
import { clearStorages, getToken } from './storage';
import { ROUTES } from '../config';

const BASIC_AUTH = {
  Authorization: '',
};
const BEARER_AUTH = { Authorization: `Bearer ${getToken()}` };

const fetch = (url, method, param1, param2) => {
  return new Promise((resolve, reject) => {
    axios[method](url, param1, param2)
      .then((res) => resolve(res.data))
      .catch((err) => {
        const defaultError = {
          code: 500,
          status: 'error',
          message: 'Failed to fetch data. Please contact developer.',
        };

        if (!err.response) reject(defaultError);
        else if (err.response.status === 401) {
          reject(err.response);
          clearStorages();
          location.href = ROUTES.login;
        } else if (!err.response.data) reject(defaultError);
        else reject(err.response.data);
      });
  });
};
