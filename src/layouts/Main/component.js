import React, { Fragment } from 'react';
import Navigation from './lib/Navigation';

export default function component(props) {
  const { children, classes } = props;
  return (
    <div className={classes.body}>
      <Navigation>{children}</Navigation>
    </div>
  );
}
