export const profileItem = [
  {
    title: 'Friends',
    path: '/friends',
    icon: 'friends',
  },
  {
    title: 'Dashboard',
    path: '/',
    icon: 'dashboard',
  },
];

export const playgroundItem = [
  {
    title: 'MyGames',
    path: '/mygames',
    icon: 'mygames',
  },
  {
    title: 'History',
    path: '/history',
    icon: 'history',
  },
  {
    title: 'Achievement',
    path: '/achievement',
    icon: 'achievement',
  },
  {
    title: 'Leaderboard',
    path: '/leaderboard',
    icon: 'leaderboard',
  },
];

export const otherItem = [
  {
    title: 'Setting',
    path: '/setting',
    icon: 'setting',
  },
  {
    title: 'Logout',
    path: '/logout',
    icon: 'logout',
  },
];
