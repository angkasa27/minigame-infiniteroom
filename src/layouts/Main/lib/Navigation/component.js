import React from 'react';
import clsx from 'clsx';
import { useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import NavIcon from '../../../../components/elements/NavIcon';

import { profileItem, playgroundItem, otherItem } from './constant';

export default function Navigation({ children, classes }) {
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const renderAppBar = () => {
    return (
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.hide]: open,
        })}
      >
        <Toolbar>
          <IconButton
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
    );
  };

  const renderDrawer = () => {
    return (
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </div>
        {renderList(profileItem)}
        <p
          className={clsx({
            [classes.hide]: !open,
          })}
          style={{ marginLeft: '16px' }}
        >
          Playground
        </p>
        {renderList(playgroundItem)}
        {renderList(otherItem)}
      </Drawer>
    );
  };

  const renderList = (item) => {
    return item.map((item, index) => (
      <ListItem
        button
        key={index}
        className={clsx(classes.item, {
          [classes.iconCLoseCenter]: !open,
        })}
      >
        <ListItemIcon>{renderIcon(item)}</ListItemIcon>
        <ListItemText primary={item.title} />
      </ListItem>
    ));
  };

  const renderIcon = (item) => {
    const location = window.location.pathname;
    const active = location.includes(item.path);
    return <NavIcon active={active} type={item.icon} />;
  };

  return (
    <div className={classes.root}>
      {renderAppBar()}
      {renderDrawer()}
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {children}
      </main>
    </div>
  );
}
